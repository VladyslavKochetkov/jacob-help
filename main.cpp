#include <iostream>
#include <cstdlib>
#include <time.h>
#include <vector>

using namespace std;

bool usedCards[53]; //Do not use 0 false = used, true = avaiable
bool isFirstCard = true;
int cardsPlayed = 0;
class Person; //prototype

class Gunslinger{
     public:
    int numOfNotches = 0;
    int Draw(){
        return (rand() % 10) + 1;
    }
};

class CardSharp{ // this has to run dunkLevel amount of %
     public:
    int Draw(){
        //returns draw time
        return (rand() % 10) + 6; // Will return 5-15
    }
    int Play(){
        int picked = ((rand() % 52) + 1);
        //check if this card is picked, if so use it
        if(usedCards[picked]){
            return picked;
        }
        //cheat up
        for(int i = picked + 1; i < 53; i++){
            if(usedCards[i]){
                return i;
            }
        }
        //noithing up to cheat cheat down
        for(int i = picked - 1; i > 0; i--){
            if(usedCards[i]){
                return i;
            }
        }
        return 0; //no cards have been played yet, cannot cheat
    }
};

class Bartender{
     public:
    bool Draw(){
        return true;
    }
};

class PokerPlayer{
     public:
    int Play(){
        int picked = ((rand() % 52) + 1);
        //Set it so if the card is used you pick a lower number (in a real poker game you cant pick a used card)
        if(!usedCards[picked]){
            usedCards[picked] = true;
            return picked;
        }
        //Picked next highest card
        for(int i = picked + 1; i < 53; i++){
            if(!usedCards[i]){
                usedCards[i] = true;
                return i;
            }
        }
        //Picks next lowest card
        for(int i = picked - 1; i > 0; i++){
            if(!usedCards[i]){
                usedCards[i] = true;
                return i;
            }
        }
        //No more cards in the deck youre fucked
        return 0;
    }
};


class Person: public PokerPlayer, public Gunslinger, public CardSharp, public Bartender{
     public:
    friend ostream& operator<<(std::ostream& os, const Person& person){ // overloads the << operator
        os << "name: " << person.name << endl <<
               "drunkLevel: " << person.drunkLevel << endl <<
               "hasPouredDrink: " << person.hasPouredDrink << endl <<
               "isDead: " << person.isDead << endl <<
               "cardValue: " << person.cardValue << endl <<
               "handsWon: " << person.handsWon << endl <<
               "playerNum: " << person.playerNum << endl <<
               "numOfNotches: " << person.numOfNotches << endl;
    }
    string name;
    int drunkLevel = 25;
    bool hasPouredDrink = false;
    bool isDead = false;
    int cardValue = 0;
    int handsWon = false;
    int playerNum = 0;
    string randomName[10] = {"Vlad", "Alex", "Jacob", "Kate", "Ivan", "Saltwick", "Amanda", "Sarah", "Sadik", "Anis"};

    Person(){ //can be used to init and reset the data
        name = randomName[rand() % 10];
        drunkLevel = 25;
        hasPouredDrink = false;
        cardValue = 0;
        handsWon = 0;
    }

    void Drink(){
        if((rand() % 10) < 10){  // 10% chance to run
            if(hasPouredDrink){ //if bartender poured a drink, drink it
                hasPouredDrink = false; //no more drink ='(((
                drunkLevel += 5; //increase drunk level
            }
        }
    }

    void Play(vector<Person> players){ //runs all the functions that go into a turn
        if(!hasPouredDrink){ //if person has no drink pour one
            hasPouredDrink = Bartender::Draw();
            cout << "The bartender poured " << name << " a drink." << endl;
        }
        if(rand() % 100 < 10){ //10% chance to actually take a drink
            cout << name << " took a drink." << endl;
        }
        if((rand() % 100 < drunkLevel)  && !isFirstCard){ //more drinks = higher chance to cheat and dont be a dumbass and cheat on first turn
            cout << name << " had a bit too much to drink and tries to cheat!" << endl;
            cardValue = CardSharp::Play(); //gets a card value from cardsharp
            if((rand() % 100 < 50)){ //will get caught 50% of the time
                int randomPlayer = playerNum; //get current player number
                int randomPersonDraw = Gunslinger::Draw(); //a gunslingers draw speed
                int thisPlayersDraw = CardSharp::Draw(); //get own draw speed
                while(randomPlayer == playerNum){ //get a random play to be gunslinger that isnt this person
                    randomPlayer = rand() % 4; //pick at random, pick again if got same person
                }
                cout << players[randomPlayer].name << " caught " << name << " cheating!" << endl;
                if(randomPersonDraw < thisPlayersDraw){ //if random person is faster than this person
                    players[randomPlayer].numOfNotches++; //add 1 to random person notch count
                    isDead = true; //set dead to true because rip because shot =(
                    cout << players[randomPlayer].name << " drew his gun first and shot " << name << "!" <<endl;
                    cout << players[randomPlayer].name << " now has " << players[randomPlayer].numOfNotches << " notches"
                            " on his gun." << endl;
                    cout << name << " died.";
                    string oldName = players[playerNum].name; //set current name to a new var
                    do{
                        players[playerNum] = Person(); //reset data and get a new name
                    }while(players[playerNum].name == oldName); //make sure new name isnt that same as old name for confusion reasons
                    cout << "Welcome " << players[playerNum].name << " to the game." << endl;
                }else{ //this person is faster than the random
                    numOfNotches++; // add 1 to this persons notches
                    players[randomPlayer].isDead = true; //random dude be rip
                    cout << players[randomPlayer].name << " tried to shoot " << name << " but " << name << "drew his "
                            "gun faster and" << players[randomPlayer].name << " got shot!" << endl;
                    cout << name << " now has " << numOfNotches << " notches on his gun. " << endl;
                    cout << players[randomPlayer].name << " died.";
                    string oldName = players[randomPlayer].name; //set current name to a new var
                    do{
                        players[randomPlayer] = Person(); //reset data and get a new name for poor random dude
                    }while(players[randomPlayer].name == oldName); //make sure names arent the same
                    cout << "Welcome " << players[randomPlayer].name << " to the game." << endl;
                }
            }else{
                cout << name << " gets away with cheating this time. He got the card " << cardValue << endl; //got away with cheating...lucky bastard
            }
        }else{
            isFirstCard = false; //no longer the first turn, cheat at your own risk
            cardValue = PokerPlayer::Play(); //if you didnt cheat just get a random unpicked card
            cout << name << " draws the card " << cardValue << "." << endl;
        }
    }
};

vector<Person> players; //set dynamically allocated memory of persons
int main(){
    srand(time(NULL)); //randomise the randomness
    cout << "Current Players:" << endl;
    for(int i = 0; i < 4; i++){     //init players and print current players
        players.push_back(Person()); //add a new person
        players[i].playerNum = i; //set persons name to assign order
        cout << "\t" << players[i].name << endl; //print everyones names
    }
    int amountOfTurns = 1; //have to start with second play to simplify logic shit
    while(cardsPlayed < 52){ //game cant last for longer than there are cards right?
        int currentTurn = amountOfTurns % 4; //loop through all players
        players[currentTurn].Play(players); //call play and pass players vectors so you can keep track of everyone
        if(currentTurn == 0){ //check winner of hand each time you get through one loop
            int roundWinner = 0; //basic logic shit for winner
            int roundWinnerCardPoints = players[roundWinner].cardValue;
            if(players[1].cardValue > roundWinnerCardPoints){
                roundWinner = 1;
                roundWinnerCardPoints = players[roundWinner].cardValue;
            }
            if(players[2].cardValue > roundWinnerCardPoints){
                roundWinner = 2;
                roundWinnerCardPoints = players[roundWinner].cardValue;
            }
            if(players[3].cardValue > roundWinnerCardPoints){
                roundWinner = 3;
                roundWinnerCardPoints = players[roundWinner].cardValue;
            }
            players[roundWinner].handsWon++; //add one hand to winner
            cout << endl << "#$#$" << players[roundWinner].name << " won this hand!$#$#" << endl;
            players[0].cardValue = 0;
            players[1].cardValue = 0;
            players[2].cardValue = 0;
            players[3].cardValue = 0;
        }
        cout << endl << endl;
        amountOfTurns++; //loop to next person
    }
    int playerWithMost = 0; //same logic shit
    int currentMost = players[0].handsWon;
    if(players[1].handsWon > currentMost){
        currentMost = players[1].handsWon;
        playerWithMost = 1;
    }
    if(players[2].handsWon > currentMost){
        currentMost = players[2].handsWon;
        playerWithMost = 2;
    }
    if(players[3].handsWon > currentMost){
        currentMost = players[3].handsWon;
        playerWithMost = 3;
    }
    cout << players[playerWithMost].name << " won the game with the most hands [" << players[playerWithMost].handsWon
                                                                                  << "]." << endl;
}